// Fill out your copyright notice in the Description page of Project Settings.

#include "BirthdaySimm.h"
#include "GameSettings.h"



UGameUserSettings* UGameSettings::getGameUserSettings() {
	if (GEngine != nullptr) {
		return GEngine->GetGameUserSettings();
	}
	return nullptr;
}


bool UGameSettings::getSupportedScreenResolutions(TArray<FIntPoint>& resolutions) {
	
	FScreenResolutionArray resArray;

	
	if (RHIGetAvailableResolutions(resArray, true)) {

		for (const FScreenResolutionRHI& res: resArray ){

			resolutions.AddUnique(FIntPoint(res.Width, res.Height));
		}

		return true;
	}

	return false;
}

bool UGameSettings::setScreenResolution(FIntPoint res, bool windowed) {

	UGameUserSettings* settings = getGameUserSettings();

	if (!settings) { return false; }

	EWindowMode::Type windowedMode = windowed ? EWindowMode::Windowed : EWindowMode::Fullscreen;

	//UE_LOG(LogTemp, Warning, TEXT("El puto bool es = %d"), windowed);

	settings->SetScreenResolution(res);
	settings->SetFullscreenMode(windowedMode);
	settings->RequestResolutionChange(res.X, res.Y, windowedMode, false);

	settings->SaveSettings();

	return true;

}

FIntPoint UGameSettings::getCurrentResolution() {
	UGameUserSettings* settings = getGameUserSettings();

	if (!settings) { return FIntPoint(0,0); }

	return settings->GetScreenResolution();
}


bool UGameSettings::isWindowed() {


	UGameUserSettings* settings = getGameUserSettings();

	if (!settings) {return false;}
	return settings->GetFullscreenMode() == EWindowMode::Windowed;
}