// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameSettings.generated.h"

/**
 * 
 */
UCLASS()
class BIRTHDAYSIMM_API UGameSettings : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

	public:

		UFUNCTION(BlueprintPure, Category = "Video Settings")
		static bool getSupportedScreenResolutions(TArray<FIntPoint>& resolutions);

		UFUNCTION(BlueprintPure, Category = "Video Settings")
		static FIntPoint getCurrentResolution();

		UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool isWindowed();

		UFUNCTION(BlueprintCallable, Category = "Video Settings")
		static bool setScreenResolution(FIntPoint newRes, bool windowed);

	private:
		static UGameUserSettings* getGameUserSettings();//Pillar User Settings
	
};
